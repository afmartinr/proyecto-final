from flask import Flask, render_template, request, jsonify
import sqlite3

class Mensaje:
    message = ''
    records = []    


# inicializacion
app = Flask(__name__)

# SQLite 
rutaDb = 'bdatos.db'
tableName = 'historial'

# routes
@app.route('/financial-app')
@app.route('/')
def Index():      
    return render_template('index.html')

@app.route('/ingresos')
def ingresos():      
    return render_template('ingresos.html')

@app.route('/gastos')
def gastos():      
    return render_template('gastos.html')

@app.route('/readData')
def read_data():
    database = sqlite3.connect(rutaDb)
    cur = database.cursor()    
    cur.execute('SELECT * FROM ' + tableName)
    data = cur.fetchall()
    cur.close()   
    listado = []

    for registro in data:
        t = {
            "id":registro[0], 
            "fecha": registro[1],
            "descripcion": registro[2],
            "valor":registro[3], 
            "tipo": registro[4],
            "origen": registro[5]
        }
        listado.append(t)
    mensaje = {"historial":listado}
    r = jsonify(mensaje)    
    return r

@app.route('/addData', methods=['POST'])
def addData():    
    if request.method == 'POST':

        date = request.form['date']
        name = request.form['name']
        value = request.form['value']
        origin = request.form['origin']
        typ = request.form['type']

        print("name: " + name + "    origin: " + origin)

        database = sqlite3.connect(rutaDb)
        cur = database.cursor()           
        cur.execute("INSERT INTO "+ tableName +" (fecha, concepto, valor, tipo, origen) VALUES (?,?,?,?,?)", (date, name, value, typ, origin))
        database.commit()
        cur.close()
        return jsonify(message = 'Data Added successfully')
    return jsonify(message = 'Error en add')

@app.route('/searchTypeOfData', methods=['POST'])
def searchData():    
    if request.method == 'POST':
        tipo = request.form['tipo']
        print("tipo: " + tipo)

        database = sqlite3.connect(rutaDb)
        cur = database.cursor()           
        cur.execute("SELECT * FROM " + tableName + " WHERE tipo=\"" + tipo + "\"")
        data = cur.fetchall()
        cur.close()
        listado = []

        for registro in data:
            t = {
                "id":registro[0], 
                "fecha": registro[1],
                "descripcion": registro[2],
                "valor":registro[3], 
                "tipo": registro[4],
                "origen": registro[5]
            }
            listado.append(t)
        mensaje = {"historial":listado}
        r = jsonify(mensaje)    
        return r
    return jsonify(message = 'Error en add')

@app.route('/delData', methods=['POST'])
def del_data():    
    #fname = request.args.get('id')  # para GET
    if request.method == 'POST':        
        registro = request.form.get('id')
        
        print("Eliminar P => ", registro)
        database = sqlite3.connect(rutaDb)
        cur = database.cursor()         
        cur.execute('DELETE FROM ' + tableName + ' WHERE id = {0}'.format(registro))
        database.commit()
        cur.close()   
        return jsonify(message = 'Contact Deleted successfully')
    return jsonify(message = 'Error en borrar')

@app.route('/globales')
def porcentajes():
    database = sqlite3.connect(rutaDb)
    cur = database.cursor()    
    
    cur.execute("SELECT SUM("+ tableName +".valor) as sumaIngresos FROM " + tableName + " WHERE tipo=\"Ingreso\"")
    suma_ingresos = cur.fetchall()[0][0]
    
    cur.execute("SELECT SUM("+ tableName +".valor) as sumaIngresos FROM " + tableName + " WHERE tipo=\"Gasto\"")
    suma_gastos = cur.fetchall()[0][0]
    
    cur.close()
    
    total = suma_ingresos - suma_gastos
    # total -> 100
    # sum -> n
    porcentaje_ingresos = int((suma_ingresos * 100) / total)
    porcentaje_gastos = int((suma_gastos * 100) / total)

    dic = {"Ingresos Netos": total,
            "Total Gastos": suma_gastos,
            "Total Ingresos": suma_ingresos,
            "Porcentaje Ingresos": porcentaje_ingresos,
            "Porcentaje Gastos": porcentaje_gastos}
    return jsonify(dic)

# inicio de la app
if __name__ == "__main__":
    app.run(port=8080, debug=True)

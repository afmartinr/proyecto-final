console.log("Activado");

// Fetching Tasks
function fetchTasks() {
    var mensaje;

    var cabecera = new FormData();
    cabecera.append("tipo", "Ingreso");

    var miInit = {
        method: 'POST',
        mode: 'cors',
        body: cabecera,
    };

    fetch('searchTypeOfData', miInit)
    .then((response) => {
        var contentType = response.headers.get("content-type");
        if (contentType && contentType.indexOf("application/json") !== -1) {
            return response.json();
        } else {
            mensaje = 'La respuesta no es un JSON \n' + response.text();
            console.log(mensaje);
        }
    })
    .catch((error) => {
        mensaje = 'Hubo un problema con la petición Fetch en RESPONSE:' + error.message;
        console.log(mensaje);
    })
    .then((data) => {
        if(data["historial"].length <= 0){
            mensaje = "No existen datos ";
        } else{
            visualizarData(data);
            mensaje = "<h3>Datos</h3><br>" + JSON.stringify(data);
        }
        console.log(mensaje);
    })
    .catch((error) => {
        mensaje = 'Hubo un problema con la petición Fetch en DATOS:' + error.message;
        console.log(mensaje);
    });
}

function visualizarData(data) {
    console.log(data);
    const gastos = data.historial;
    let template = '';

    gastos.forEach(gastos => {
        template += `
                <tr taskId="${gastos.id}">
                    <td>${gastos.id}</td>
                    <td>${gastos.fecha}</td>
                    <td>${gastos.descripcion}</td>
                    <td>${gastos.valor}</td>
                    <td>${gastos.origen}</td>
                    <td>
                        <button class="gastos-delete btn btn-danger" onclick="eliminarTarea()">
                        Eliminar 
                        </button>
                    </td>
                </tr>
            `
    });
    document.querySelector('#ingresos').innerHTML = template;
}

function eliminarTarea() {
    if (confirm('¿Está seguro que quiere eliminar este gasto?')) {
        const element = document.activeElement.parentElement.parentElement;
        const id = element.attributes.taskId.value;
        console.log(id);

        const data = new FormData();
        data.append('id', id);

        fetch('delData', {
            method: 'POST',
            body: data
        }).then((response) => {
            var contentType = response.headers.get("content-type");
            if (contentType && contentType.indexOf("application/json") !== -1) {
                return response.json();
            } else {
                mensaje = 'La respuesta no es un JSON \n' + response.text();
                console.log(mensaje);
            }
        }).catch((error) => {
            mensaje = 'Hubo un problema con la petición Fetch en RESPONSE:' + error.message;
            console.log(mensaje);
        }).then((data) => {
            mensaje = "<h3>Datos</h3><br>" + JSON.stringify(data);
            //fetchTasks();
            location.reload();
        }).catch((error) => {
            mensaje = 'Hubo un problema con la petición Fetch en DATOS:' + error.message;
            console.log(mensaje);
        });
    }
}

fetchTasks();
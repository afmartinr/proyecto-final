console.log('Activado');

var formulario = document.querySelector('#ingreso-form');
formulario.addEventListener('submit', e=>{
    console.log("submit");
    e.preventDefault();

    const data = new FormData();
    data.append('date', document.getElementById('date').value);
    data.append('name', document.getElementById('name').value);
    data.append('value', document.getElementById('value').value);
    data.append('type', document.getElementById('type').value);
    data.append('origin', document.getElementById('origin').value);

    fetch('addData', {
        method: 'POST',
        mode: 'cors',
        body: data
    }).then((response) =>{
        var contentType = response.headers.get('content-type');
        if(contentType && contentType.indexOf("application/json") !== -1){
            return response.json();
        } else {
            mensaje = "La respuesta no es un JSON perro"
            console.log(mensaje);
        }
    }).catch((error) =>{
        mensaje = "Hubo un error con la petición Fetch en RESPONSE" + error;
        console.log(mensaje);
    }).then((data) => {
        mensaje = "<h3>Datos</h3><br>" + JSON.stringify(data);
        console.log(mensaje);
        alert("Se agrego "+ document.getElementById("name").value);
        location.reload();
    }).catch((error) =>{
        mensaje = "Hubo un error con la petición Fetch en DATA" + error.json();
        console.log(mensaje);
    })
})

function porcentajes(){
    var cabecera = new Headers();
    cabecera.append("Content-Type", "application/json");

    var miInit = { method: 'GET',
                headers: cabecera,
                mode: 'cors',
                cache: 'default' };
 
    fetch("globales", miInit)
        .then((response) =>{
            var contentType = response.headers.get("content-type");
            if(contentType && contentType.indexOf("application/json") !== -1) {
              return response.json();
            } else {
              mensaje = 'La respuesta no es un JSON \n' + response.text(); 
              console.log(mensaje);
            }    
        }).catch((error)=>{
            mensaje = 'Hubo un problema con la petición Fetch en RESPONSE:' + error.message; 
            console.log(mensaje);   
        }).then((data) =>{
            document.getElementById("mostrardatos").innerHTML = 
                    '<h1>Gastos Totales</h1>$ ' + data["Total Gastos"]
                    + '<h1>Ingresos Totales</h1>$ ' + data["Total Ingresos"]
                    + '<h1>Porcentaje de gastos:</h1>' + data["Porcentaje Gastos"] + "% <br>" 
                    + '<h1>Ingresos Netos:</h1>$ ' + data["Ingresos Netos"] 
                
        }).catch((error)=>{
            mensaje = 'Hubo un problema con la petición Fetch en DATOS:' + error.message; 
            console.log(mensaje);   
        })
}

porcentajes();
